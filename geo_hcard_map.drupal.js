/**
 * @file
 * Javascript geo_hcard_map file
 *
 * looks for a DIV @id=geo-hcard-map to display a map in
 * and plots all hCrad points in the webpage
 */
(function ($) {
  'use strict';
  Drupal.behaviors.leaflet = {
    attach: function (context, settings) {

      var locationsMap;
      var vcards;
      var markers;
      var group;
      var padding;
      var mapDIV = $('#geo-hcard-map');

      if (!window.geo_hcard_map_settings) {
        window.geo_hcard_map_settings = {geo_hcard_map_type: 'osm'};
      }

      if (mapDIV.length) {
        switch (window.geo_hcard_map_settings.geo_hcard_map_type) {
          case 'osm': {
            if (window.L) {
              locationsMap = L.map(mapDIV[0]).setView([51.505, -0.09], 13);
              markers = [];

              // OSM streets
              L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="https://opendatacommons.org/licenses/odbl/1.0/">ODbL</a>',
                maxZoom: 18
              }).addTo(locationsMap);

              // Add hCard's
              vcards = $('.vcard');
              vcards.each(function () {
                var marker;
                var Icon;
                var oOptions = {};
                var oIconOptions = {};
                var adr = $(this).find('.adr:first');
                var lat = adr.find('.geo .latitude').text();
                var lng = adr.find('.geo .longitude').text();
                var iconUrl = adr.find('.geo .icon').text();
                var iconShadowUrl = adr.find('.geo .icon-shadow').text();
                var title = $(this).find('.fn:first').text();
                var href = $(this).find('.fn:first').attr('href');
                var desc = '';

                $(this).find('.cb-popup').each(function () {
                  desc += $('<div/>').append($(this).clone()).html();
                });

                if (lat && lng) {
                  // Give some defaults for best chances of working
                  if (!title) {
                    title = '(no title)';
                  }
                  if (!href) {
                    href = '#' + title; // Should not happen
                  }

                  if (iconUrl) {
                    oIconOptions = {
                      iconUrl: iconUrl,
                      title: title,
                      alt: title,
                      iconSize: [48, 48], // size of the icon
                      shadowSize: [48, 48], // size of the shadow
                      iconAnchor: [24, 24], // point of the icon which will correspond to marker's location
                      shadowAnchor: [20, 20], // the same for the shadow
                      popupAnchor: [0, -8] // point from which the popup should open relative to the iconAnchor
                    };
                    if (iconShadowUrl) {
                      oIconOptions.shadowUrl = iconShadowUrl;
                    }
                    Icon = L.Icon.extend({options: oIconOptions});
                    oOptions.icon = new Icon();
                  }

                  marker = L.marker([lat, lng], oOptions).addTo(locationsMap);
                  marker.bindPopup('<h2><a href="' + href + '">' + title + '</a></h2>' + desc);
                  markers.push(marker);
                }
              });

              // Fit to all markers
              if (markers.length) {
                padding = (markers.length === 1 ? 2 : 0.5);
                group = new L.featureGroup(markers);
                locationsMap.fitBounds(group.getBounds().pad(padding));
              }
            }
            break;
          }
        }
      }
    }
  };

})(jQuery);
