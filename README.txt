GEO HCARD MAP - http://drupal.org/module/geo_hcard_map
======================================================
Contributors: anewholm
Tags: map, location, OSM, leaflet, hCard, vCard
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

DESCRIPTION
------------
Map of [hCard](http://microformats.org/wiki/hcard) elements 
found in the current webpage.
All `.cb-popup` sub-elements will be included in the map popup 
for the corresponding `.vcard` item. 
See below for an example of a 
[hCard](http://microformats.org/wiki/hcard) element.
The developer must ensure the correct 
[hCard](http://microformats.org/wiki/hcard) markup in the post templates.

EXAMPLE HCARD MARKUP
------------
    <div class="vcard">
      <h2><a class="fn org" href="http://example.com/magnet-bank/">Mag</a></h2>
      <div class="cb-popup">Balacs utca, 1000 Budapest, Hungary</div>
      <div class="cb-popup">Mon-Fri, 8:00 - 18:00</div>
      <div class="adr">
        <div class="geo">
          <span class="latitude">47.50496815665008</span>,
          <span class="longitude">19.063553810119632</span>
          <span class="icon">
            /sites/all/modules/geo_hcard_map/images/spanner.png
          </span>
          <span class="icon-shadow"></span>
        </div>
      </div>
    </div>

[microformats wiki](http://microformats.org/wiki/hcard) 
and [geo](http://microformats.org/wiki/geo)

INSTALLATION
------------

1. Download and install the Module
2. Upload the module folder to the '/sites/all/modules/' directory
3. Enable the module through the 'Module' menu in Drupal
4. Include the appropriate hcard markup in your template output
5. Show the map with a DIV with @id=geo-hcard-map

SCREENSHOTS
------------
In /assets/

1. Example

CHANGELOG
------------

1.0
* Birth.
